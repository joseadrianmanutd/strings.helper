﻿using System;
using System.Collections.Generic;

namespace Strings.Helper
{
    public static class StringExtensions
    {
        public static bool IsPalindrome(this string value)
        {
            if (string.IsNullOrWhiteSpace(value))
            {
                return false;
            }

            for (int i = 0; i < value.Length - 1; i++)
            {                
                if (char.ToLower(value[i]) != char.ToLower(value[value.Length - 1 - i]))
                {
                    return false;
                }
            }

            return true;
        }

        public static string[] SplitByChar(this string value, char separator)
        {
            if (string.IsNullOrWhiteSpace(value))
            {
                throw new NullReferenceException();
            }

            var result = new List<string>();
            var restOfString = value;

            while (true)
            {
                var indexOfNextSeparator = restOfString.IndexOf(separator);
                if (indexOfNextSeparator == -1)
                {
                    result.Add(restOfString);
                    break;
                }

                result.Add(restOfString.Substring(0, indexOfNextSeparator));
                restOfString = restOfString.Substring(indexOfNextSeparator + 1);               
            }

            return result.ToArray();
        }
    }
}
