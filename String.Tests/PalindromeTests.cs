using NUnit.Framework;
using Strings.Helper;

namespace String.Tests
{
    public class PalindromeTests
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void StringExtensions_Return_True_When_Is_Palindrome()
        {
            var value = "Hannah";
            var isPalindrome = value.IsPalindrome();
            Assert.That(isPalindrome, Is.EqualTo(true));
        }

        [Test]
        public void StringExtensions_Return_False_When_Is_Not_Palindrome()
        {
            var value = "Hello";
            var isPalindrome = value.IsPalindrome();
            Assert.That(isPalindrome, Is.EqualTo(false));
        }

        [Test]
        public void StringExtensions_Return_False_When_String_Is_Null()
        {
            string value = null;
            var isPalindrome = value.IsPalindrome();
            Assert.That(isPalindrome, Is.EqualTo(false));
        }
    }
}