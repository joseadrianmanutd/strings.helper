using NUnit.Framework;
using Strings.Helper;

namespace String.Tests
{
    public class StringSplitTests
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void StringExtensions_Return_Array_Split_By_Comma()
        {
            var value = "test,test1,test2,test3";
            var result = value.SplitByChar(',');
            Assert.That(result.Length, Is.EqualTo(4));
        }

        [Test]
        public void StringExtensions_Return_Array_OneString_Split_By_Comma()
        {
            var value = "test";
            var result = value.SplitByChar(',');
            Assert.That(result.Length, Is.EqualTo(1));
        }

        [Test]
        public void StringExtensions_Return_Array_Split_By_Comma_When_Null()
        {
            string value = null;
            try
            {
                var result = value.SplitByChar(',');
                Assert.True(false);
            }
            catch
            {
                Assert.True(true);
            }
        }

        [Test]
        public void StringExtensions_Return_Array_OneString_End_And_Start_With_Comma()
        {
            var value = ",test,";
            var result = value.SplitByChar(',');
            Assert.That(result.Length, Is.EqualTo(3));
        }
    }
}